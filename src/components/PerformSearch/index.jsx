import React, { useEffect } from 'react';
import swal from 'sweetalert2';
import PropTypes from 'prop-types';

import { useRetPKMN } from '../../utils/search';
import { useStore, SET_PKMN } from '../../utils/store';

function PerformSearch({ pkmn_name, close_fn }) {
  const [error, isLoading, response] = useRetPKMN(pkmn_name);
  const { dispatch } = useStore();

  useEffect(() => {
    if (error) {
      swal.fire('Uh Oh...', 'An error occured. Please try again later', 'error');
      close_fn();
    }

    if (response) {
      console.log(response);
      dispatch({ type: SET_PKMN, pkmn: response });
      close_fn();
    }
  }, [error, isLoading, response, close_fn, dispatch]);

  return <div/>;
}

PerformSearch.propTypes = {
  pkmn_name: PropTypes.string.isRequired,
  close_fn: PropTypes.func.isRequired,
};

export default PerformSearch;
