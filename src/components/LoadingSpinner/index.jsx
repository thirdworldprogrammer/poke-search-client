import styled, { keyframes } from 'styled-components/macro';

const rotate = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

const LoadingSpinnerDiv = styled.div`
  display: inline-block;
  width: 64px;
  height: 64px;
  animation: ${rotate} 2s linear infinite;

  &:after {
    content: " ";
    display: block;
    width: 46px;
    height: 46px;
    margin: 1px;
    border-radius: 50%;
    border: 5px solid #aaa;
    border-color: #aaa transparent #aaa transparent;
    animation: ${rotate} 1.2s linear infinite;
  }
`;

export default LoadingSpinnerDiv;
