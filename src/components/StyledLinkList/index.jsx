import React from 'react';
import styled from 'styled-components/macro';

import StyledLink from '../StyledLink';
import { DeadUl, DeadLi } from '../DeadListComponents';

const LinkUl = styled(DeadUl)``;
const LinkLi = styled(DeadLi)`
  margin-bottom: 0.55rem;
`;


function StyledLinkList({ items }) {
  return (
    <LinkUl>
      {items.map(({ link, text }) => (
        <LinkLi>
          <StyledLink link={link} text={text} />
        </LinkLi>
      ))}
    </LinkUl>
  );
}

export default StyledLinkList;
