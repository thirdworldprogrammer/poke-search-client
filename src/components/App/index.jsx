import React from 'react';
import { Router } from 'react-router-dom';
// import styled from 'styled-components/macro';

import Navbar from '../Navbar';
import AppRouter from '../AppRouter';
// import Searchbar from '../Searchbar';

import { StoreProvider } from '../../utils/store';
import { history } from '../../utils/history';

function App() {
  return (
    <StoreProvider>
      <div className="App">
        <Router history={history}>
          <Navbar />
          <main className="main_space">
            <AppRouter />
          </main>
        </Router>
      </div>
    </StoreProvider>
  );
}

export default App;
