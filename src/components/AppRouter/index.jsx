import React from 'react';
import { Switch, Route } from 'react-router-dom';

import MainPage from '../../pages/main';
import AboutPage from '../../pages/about';

function AppRouter() {
  return (
    <Switch>
      <Route component={MainPage} path="/" exact />
      <Route component={AboutPage} path="/about" exact />
    </Switch>
  );
}

export default AppRouter;
