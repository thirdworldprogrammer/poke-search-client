// import React from 'react';
import styled from 'styled-components';

const DeadUl = styled.ul`
  margin: 0;
  padding: 0;
  list-style-type: none;
`;

const DeadLi = styled.li`
`;

// export default DeadList

export {
  DeadUl,
  DeadLi,
};
