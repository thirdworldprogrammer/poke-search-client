import React from 'react';
import styled, { css } from 'styled-components/macro';
import PropTypes from 'prop-types';

import { extractStats } from '../../utils/transforms';

const table_borders = css`
  border: 1px #bbb;
`;

const differential_alignment = css`
  text-align: ${props => props.value ? 'right': 'left'};
`;

const StatsContainer = styled.div`
  background-color: #222;
  color: #ddd;
  padding: 0.5rem;
`;

const StatsTable = styled.table`
  ${table_borders}
`;

const StatsTBody = styled.tbody`
`;

const StatsTH = styled.th`
  ${table_borders}
  ${differential_alignment}
`;

const StatsTR = styled.tr`
  ${table_borders}
  /* text-align: left; */
`;

const StatsTD = styled.td`
  /* text-align: ${props => props.value ? 'right': 'left'}; */
  ${differential_alignment}
  text-transform: capitalize;
`;

function StatsChart({ stats_original }) {
  const stat_names = ["attack", "special-attack", "defense", "special-defense", "speed"];
  const proper_stat_mapping = extractStats(stats_original);

  return (
    <StatsContainer>
      <StatsTable>
        <StatsTBody>
          <StatsTR>
            <StatsTH>
              Stat
          </StatsTH>
            <StatsTH value>
              Base Value
          </StatsTH>
          </StatsTR>
          {
            stat_names.map((name) => (
              <StatsTR key={name}>
                <StatsTD>{name}</StatsTD>
                <StatsTD value>{proper_stat_mapping[name]}</StatsTD>
              </StatsTR>
            ))
          }
        </StatsTBody>
      </StatsTable>
    </StatsContainer>
  );
}

StatsChart.propTypes = {
  stats_original: PropTypes.array.isRequired,
};

export default StatsChart;
