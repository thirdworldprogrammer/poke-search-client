import styled from 'styled-components/macro';

const BasicPanel = styled.section`
  border-radius: 0.25rem;
  background-color: #fff;
  padding: 1rem;
  min-height: 80vh;
`;

export default BasicPanel;
