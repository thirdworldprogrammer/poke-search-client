import styled from 'styled-components/macro';

const TextualArea = styled.section`
  margin-top: 1.25rem;
  background-color: #eee;
  color: #444;
  padding: 2rem;
`;

export default TextualArea;
