import React, { useEffect, useReducer, useRef } from 'react';
import styled from 'styled-components/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
// import { debounce as Ldebounce } from 'lodash';

// import pkmn_names from '../../data/pkmn_names.json';
// console.log({ pkmn_names });

import PerformSearch from '../PerformSearch';

import { useSearchPKMN } from '../../utils/search';
import { capitalizeFirstLetter } from '../../utils/transforms';

const Wrap = styled.div`
  width: 100%;
  position: relative;
  /* position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%); */
`;

const Search = styled.div`
  width: 100%;
  position: relative;
  display: flex;
`;

const SearchTerm = styled.input`
  width: 100%;
  border: 3px solid #00B4CC;
  border-right: none;
  padding: 5px;
  height: 20px;
  border-radius: 5px 0 0 5px;
  outline: none;
  color: #9DBFAF;
  letter-spacing: 0.175rem;

  :focus {
    color: #00B4CC;
  }
`;

const SearchButton = styled.button`
  width: 40px;
  height: 36px;
  border: 1px solid #00B4CC;
  background: #00B4CC;
  text-align: center;
  color: #fff;
  border-radius: 0 5px 5px 0;
  cursor: pointer;
  font-size: 20px;
`;

const SearchResultList = styled.ul`
  background-color: rgba(248, 248, 248, 1);
  list-style-type: none;
  margin: 0;
  padding: 0;
  max-height: 400px;
  overflow-y: scroll;
  position: absolute;
  min-width: 100%;
  z-index: 10;
  border: 1px solid black;
`;

const SearchResultItem = styled.li`
  border-bottom: 0.2px solid black;
  margin: 0;
  padding: 0.5rem;
  cursor: pointer;
`;

// const Select = styled.select`
//   width: 100%;
//   height: 35px;
//   background: white;
//   color: gray;
//   padding-left: 5px;
//   font-size: 14px;
//   border: none;
//   margin-left: 10px;

//   option {
//     color: black;
//     background: white;
//     display: flex;
//     white-space: pre;
//     min-height: 20px;
//     padding: 0px 2px 1px;
//   }
// `;


const default_search_state = {
  search_term: '',
  search_focused: false,
  matching_pkmn: [],
  ret_in_progress: false,
  selected_pkmn: null,
};

const SET_SEARCH_TERM = 'SET_SEARCH_TERM';
const SET_SEARCH_FOCUSED = 'SET_SEARCH_FOCUSED';
const SET_MATCHING_PKMN = 'SET_MATCHING_PKMN';
const SET_SIGNAL_RET = 'SET_SIGNAL_RET';

function reduce_state(state = default_search_state, action) {
  switch (action.type) {
    case SET_SEARCH_TERM:
      return {
        ...state,
        search_term: action.search_term,
      };
    case SET_SEARCH_FOCUSED:
      return {
        ...state,
        search_focused: action.search_focused,
      };
    case SET_MATCHING_PKMN:
      return {
        ...state,
        matching_pkmn: action.matching_pkmn,
      };
    case SET_SIGNAL_RET:
      const { selected_pkmn } = action;
      return {
        ...state,
        ret_in_progress: action.ret_in_progress,
        selected_pkmn: action.selected_pkmn,
        search_term: selected_pkmn.length ? capitalizeFirstLetter(action.selected_pkmn) : state.search_term,
        search_focused: false,
      };
    default:
      return state;
  }
}

function Searchbar() {
  const containerRef = useRef(null);
  const [state, dispatch] = useReducer(reduce_state, default_search_state);
  const { matching_pkmn, search_focused, search_term, ret_in_progress, selected_pkmn } = state;

  const close_dropdown = (e) => {
    if (containerRef.current.contains(e.target)) {
      return;
    }
    dispatch({ type: SET_SEARCH_FOCUSED, search_focused: false });
  };

  useEffect(() => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const found_matches = useSearchPKMN(search_term);
    dispatch({
      type: SET_MATCHING_PKMN,
      matching_pkmn: found_matches,
    });
  }, [search_focused, search_term]);

  useEffect(() => {
    document.addEventListener('mousedown', close_dropdown, false);
    return () => {
      document.removeEventListener('mousedown', close_dropdown, false);
    }
  }, []);

  const handle_change = (e) => {
    const { target: { value } } = e;
    dispatch({ type: SET_SEARCH_TERM, search_term: value });
  };

  const signal_ret_open = (pkmn_name) => {
    // dispatch({ type: SET_SEARCH_TERM, search_term: capitalizeFirstLetter(pkmn_name) });
    // dispatch({ type: SET_SEARCH_FOCUSED, search_focused: false });
    dispatch({
      type: SET_SIGNAL_RET, ret_in_progress: true, selected_pkmn: pkmn_name
    });
    // dispatch({ type: SET_SEARCH_FOCUSED, search_focused: false });
  };

  const signal_ret_close = () => {
    dispatch({ type: SET_SIGNAL_RET, ret_in_progress: false, selected_pkmn: '' });
  };

  return (
    <>
      { ret_in_progress && <PerformSearch pkmn_name={selected_pkmn} close_fn={signal_ret_close} /> }
      <Wrap
        ref={containerRef}
      >
        <Search>
          <SearchTerm
            value={search_term}
            onFocus={() => dispatch({ type: SET_SEARCH_FOCUSED, search_focused: true })}
            // onBlur={() => dispatch({ type: SET_SEARCH_FOCUSED, search_focused: false })}
            onChange={handle_change}
          />
          <SearchButton>
            <FontAwesomeIcon icon={faSearch} />
          </SearchButton>
        </Search>
        {(matching_pkmn.length && search_focused) ? <SearchResultList>
          {matching_pkmn.map(name => (
            <SearchResultItem key={name} onClick={() => signal_ret_open(name)}>{capitalizeFirstLetter(name)}</SearchResultItem>
          ))}
        </SearchResultList> : null}
        
      </Wrap>
    </>
  );
}

export default Searchbar;
