import styled from 'styled-components/macro';

const PageContainer = styled.main`
  background-color: aquamarine;
  min-height: 99vh;
  display: flex;
  flex-wrap: wrap;
  /* align-items: flex-start;
  justify-content: flex-start; */
  align-content: flex-start;
  padding: 1rem;
`;

export default PageContainer;
