import React from 'react';
import { NavLink } from 'react-router-dom';
import styled, { css } from 'styled-components/macro';

// import { get_pathname } from '../../utils/history';

const centering_styles = css`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const NavContainer = styled.nav`
  background-color: teal;
  color: #eee;
  display: flex;
  min-height: 2.2rem;
`;

const AppName = styled.section`
  display: block;
  font-size: 1.5rem;
  flex: 1 1 20%;

  ${centering_styles}
`;

const NavLinksList = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
  flex: 1 1 80%;
`;

const NavLinkLi = styled.li`
  flex: 0 1 100px;
  ${centering_styles}
  transition: all 0.25s ease;

  :hover {
    background-color: #00dbdb;
  }
`;

const name_link_mapping = {
  'Home': '/',
  'About': '/about',
};

const name_link_ordering = ['Home', 'About'];

const StyledNavLink = styled(NavLink)`
  text-decoration: none;
  color: #eee;
  height: 100%;
  ${centering_styles}
  flex: 1 1 100%;
  /* display: block; */
  /* flex: 1 1 100%; */
`;

const nav_link_list = name_link_ordering.map((name) => {
  const link = name_link_mapping[name];

  return (<NavLinkLi key={name}>
    <StyledNavLink to={link}>{name}</StyledNavLink>
  </NavLinkLi>);
});

function Navbar() {
  return (
    <NavContainer>
      <AppName>
        Poke ~ Search Client
      </AppName>
      <NavLinksList>
        { nav_link_list }
      </NavLinksList>
    </NavContainer>
  );
}

export default Navbar;
