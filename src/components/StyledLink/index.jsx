import React from 'react';
import styled from 'styled-components/macro';

import { capitalizeFirstLetter } from '../../utils/transforms';

const StyledLinkItem = styled.a`
  &, &:visited, &:hover, &:active {
    -webkit-backface-visibility:hidden;
    backface-visibility:hidden;
	  position:relative;
    transition:0.5s color ease;
	  text-decoration:none;
	  color:#444;
	  font-size:1.5em;
  }

  &:hover{
	  color: aqua;
  }

  &:after{
    content: "";
    transition:0.5s all ease;
    -webkit-backface-visibility:hidden;
    backface-visibility:hidden;
    position:absolute;
    bottom:-0.25em;
    height:5px;
    height:0.35rem;
    width:0;
    background:#ccc;
    left:0;
    /* width:100%; */
  }

  &:hover:after{
    width:100%;
  }

`;

function StyledLink({ link, text }) {
  return (
    <StyledLinkItem href={link} target="open">{capitalizeFirstLetter(text)}</StyledLinkItem>
  );
}

export default StyledLink;
