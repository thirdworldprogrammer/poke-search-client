import React from 'react';
import styled, { css } from 'styled-components/macro';
import ImageLoader from 'react-load-image';

import Searchbar from '../components/Searchbar';
import Accordion from '../components/Accordion';
import StyledLinkList from '../components/StyledLinkList';
import StyledLink from '../components/StyledLink';
import LoadingSpinner from '../components/LoadingSpinner';
import StatsChart from '../components/StatsChart';
import PageContainer from '../components/PageContainer';
import BasicPanel from '../components/BasicPanel';
import TextualArea from '../components/TextualArea';

import { useStore } from '../utils/store';

const common_panel_styles = css`
  border-radius: 0.25rem;
  background-color: #fff;
  padding: 1rem;
  min-height: 80vh;
`;

const SearchPanel = styled.section`
  flex: 1 1 100%;
  margin-bottom: 1rem;
  /* max-height: 10vh; */
`;

const VisualPanel = styled(BasicPanel)`
  ${common_panel_styles}
  flex: 1 1 20%;
  margin-right: 1rem;
`;

const DetailsPanel = styled(BasicPanel)`
  ${common_panel_styles}
  flex: 1 1 70%;
`;

const SpeciesSection = styled.section`
  margin-bottom: 0.75rem;
  padding: 0.75rem;
  background-color: #aaa;
  display: inline-block;
`;

const SpeciesLabel = styled.label`
  color: #555;
  font-size: 1.25rem;
`;

const StyledImageLoader = styled(ImageLoader)`
  text-align: center;
`;

function DisplayPKMNData({ pkmn }) {
  const pkmn_forms_n_url = !pkmn ? [] : pkmn.forms.map(({ name, url }) => ({ text: name, link: url }));
  const pkmn_moves_n_url = !pkmn ? [] : pkmn.moves.map(({ move }) => ({ link: move.url, text: move.name }));
  const pkmn_games_n_url = !pkmn ? [] : pkmn.game_indices.map(({ version }) => ({ link: version.url, text: `Pokemon ${version.name}` }));

  return (
    <>
      <VisualPanel>
        {/* { !img_loaded &&  } */}
        <SpeciesSection>
          <SpeciesLabel>Species: &nbsp; </SpeciesLabel>
          <StyledLink link={pkmn.species.url} text={pkmn.species.name} />
        </SpeciesSection>
        <StyledImageLoader src={pkmn.sprites.front_default}>
          <img alt={pkmn.name} />
          <div>Error!</div>
          <LoadingSpinner />
        </StyledImageLoader>
        <StatsChart stats_original={pkmn.stats} />
      </VisualPanel>
      <DetailsPanel>
        <Accordion allowMultipleOpen>
          <div label="Forms">
            <StyledLinkList items={pkmn_forms_n_url} />
          </div>
          <div label="Moves">
            <StyledLinkList items={pkmn_moves_n_url} />
          </div>
          <div label="Games">
            <StyledLinkList items={pkmn_games_n_url} />
          </div>
        </Accordion>
      </DetailsPanel>
    </>
  );
};

function SearchInfo() {
  return (
    <TextualArea>
      <p>
        The above search bar is to be used to search for the desired pokemon. Just begin entering as much of the name as you remember,
        and you should soon find the pokemon you're looking for.
      </p>
      <p>
        Give it a try, right now!
      </p>
    </TextualArea>
  );
}

function MainPage() {
  const { state: { current_pkmn: { pkmn } } } = useStore();

  return (
    <PageContainer>
      <SearchPanel>
        <Searchbar />
      </SearchPanel>
      {
        pkmn ?
        <DisplayPKMNData pkmn={pkmn} />
        :
        <SearchInfo />
      }
    </PageContainer>
  );
}

export default MainPage;
