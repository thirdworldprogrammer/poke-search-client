import React from 'react';
import styled from 'styled-components/macro';

import PageContainer from '../components/PageContainer';
import BasicPanel from '../components/BasicPanel';
import TextualArea from '../components/TextualArea';

const AboutDetailsPanel = styled(BasicPanel)`
  width: 100%;
  min-height: 100vh;
`;

const AboutTitle = styled.h1`
  font-size: 3rem;
  text-align: center;
  color: #333;
`;

const HorizontalRule = styled.hr`
  width: 65%;
  height: 0.125rem;
  background-color: #888;
`;

function AboutPage() {
  return (
    <PageContainer>
      <AboutDetailsPanel>
        <AboutTitle>About Page</AboutTitle>
        <HorizontalRule />
        <TextualArea>
          <p>
            Thanks for using this little pet project of mine! I hope you like it. Might not be as feature filled
            as it could be, but this little app lets you quickly search through the pokeApi for pokemon.
          </p>
          <p>
            You're free to use this project and the code for it however you see fit, it's all open sourced. If you'd like to contact me,
            I'd recommend reaching me through my email. It's revarjunr@gmail.com
          </p>
          <p>
            Once again, thanks for using this small app!
          </p>
        </TextualArea>
      </AboutDetailsPanel>
    </PageContainer>
  );
}

export default AboutPage;
