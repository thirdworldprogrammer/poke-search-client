import { useState, useEffect } from 'react';

function useFetch(url, options) {
  const [response, set_response] = useState(null);
  const [error, set_error] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [running, set_running] = useState(false);

  const re_run = () => {
    set_running(true);
  };

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      set_running(false);

      try {
        const res = await fetch(url, options);
        const json = await res.json();
        set_response(json);
        setIsLoading(false)
      } catch (error) {
        set_error(error);
      }
    };
    fetchData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [running]);

  return { response, error, isLoading, re_run };
}

export {
  useFetch,
};
