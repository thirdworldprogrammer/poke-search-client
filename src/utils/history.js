import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

export function get_pathname() {
  return history.location.pathname;
}
