import React, { createContext, useReducer, useContext } from 'react';

function combineReducers(reducers) {
  // First get an array with all the keys of the reducers (the reducer names)
  const reducerKeys = Object.keys(reducers);

  return function combination(state = {}, action) {
    // This is the object we are going to return.
    const nextState = {};

    for (let i = 0; i < reducerKeys.length; i++) {
      const key = reducerKeys[i];
      const reducer = reducers[key];
      const previousStateForKey = state[key];
      const nextStateForKey = reducer(previousStateForKey, action);
      nextState[key] = nextStateForKey;
    }
    return nextState;
  }
}

const current_pkmn_default_state = {
  pkmn: null,
};

const SET_PKMN = 'SET_PKMN';
const CLEAR_PKMN = 'CLEAR_PKMN';

function current_pkmn_reducer(state = current_pkmn_default_state, action) {
  switch(action.type) {
    case SET_PKMN:
      return {
        ...state,
        pkmn: action.pkmn,
      };
    case CLEAR_PKMN:
      return current_pkmn_default_state;
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  current_pkmn: current_pkmn_reducer,
});

const StoreContext = createContext(null);
function StoreProvider({ children }) {
  const [state, dispatch] = useReducer(rootReducer, { current_pkmn: current_pkmn_default_state });
  const value = { state, dispatch };

  return <StoreContext.Provider value={value}>{children}</StoreContext.Provider>;
}
const useStore = () => useContext(StoreContext);

export {
  useStore,
  StoreProvider,
  SET_PKMN,
  CLEAR_PKMN,
};
