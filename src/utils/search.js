// import { useEffect, useState } from 'react';
import pkmn_names from '../data/pkmn_names.json';
import { useFetch } from './api';

const pokemon_ret_base_url = 'https://pokeapi.co/api/v2/pokemon';

function useSearchPKMN(search_term) {
  if (!search_term) return [];
  if (search_term && search_term.length === 0) return [];
  return pkmn_names.filter((name) => String(name).includes(String(search_term).toLocaleLowerCase()));
}

function useRetPKMN(pkmn_name_provided) {
  const { error, isLoading, response } = useFetch(`${pokemon_ret_base_url}/${pkmn_name_provided}`);
  return [error, isLoading, response];
}

export {
  useSearchPKMN,
  useRetPKMN,
};
