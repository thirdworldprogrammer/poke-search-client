import dotProp from 'dot-prop';

function capitalizeFirstLetter(string) {
  if (!string) return '';
  return string.split(' ').map((each) => each.charAt(0).toUpperCase() + each.slice(1)).join(' ');
  // return string.charAt(0).toUpperCase() + string.slice(1);
}

// Stats are : 'attack', 'defense', 'special-attack', 'special-defense', 'speed'
function extractStats(original_stats_array) {
  const final_stat_mapping = {};
  console.log({ original_stats_array });

  original_stats_array.forEach((item) => {
    const stat_name = dotProp.get(item, 'stat.name');
    final_stat_mapping[stat_name] = dotProp.get(item, 'base_stat');
  });

  return final_stat_mapping;
}

export {
  capitalizeFirstLetter,
  extractStats,
};
