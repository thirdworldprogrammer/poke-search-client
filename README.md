This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Pokemon types by: [https://www.deviantart.com/cameronwink/art/Pokemon-Type-Icon-List-475587471] # To be implemented

This project was created with create-react-app; there are a bit more of visual features and such to add
but currently it is very much usable. You can search for Pokemon using the search box, and all that you would need to do to run the app locally would be to enter: `npm run start` in the terminal, with the directory set to the project directory.

Would appreciate any and all feedback. Thanks