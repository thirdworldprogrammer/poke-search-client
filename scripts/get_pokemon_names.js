const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const csv = require('csvtojson');

const pkmn_csv_data_addr = 'https://raw.githubusercontent.com/PokeAPI/pokeapi/master/data/v2/csv/pokemon.csv';

function storeData(data, path) {
  try {
    fs.writeFileSync(path, JSON.stringify(data), { flag: 'w' });
  } catch (err) {
    throw err;
  }
}

async function get_pokemon_names() {
  try {
    const response = await fetch(pkmn_csv_data_addr);
    const body = await response.text();
    const parsed = await csv().fromString(body);
    const pokemon_names = parsed.filter(({ id }) => id <= 10003).map(({ identifier }) => identifier);
    storeData(pokemon_names, path.join(__dirname, '..', 'src', 'data', 'pkmn_names.json'));
    console.log({ pokemon_names });
  } catch (error) {
    throw error;
  }
}

get_pokemon_names();
